#pragma once

#include <stdint.h> 

class NES;

class Memory {
	public:
		Memory(NES *nes);

		void reset(void);

		void set_pressed_buttons(bool controller, bool *buttons) {
			if (controller)
				this->buttons_p2 = buttons;
			else
				this->buttons_p1 = buttons;
		}

		void stack_push(uint8_t &SP, uint8_t value);
		uint8_t stack_pull(uint8_t &SP);
		

		void write_address(uint16_t address, uint8_t value);
		uint8_t read_address(uint16_t address);
		uint16_t read_address16(uint16_t address);
		uint16_t read_address16_bug(uint16_t address);
	
		uint8_t read_address_mapper(uint16_t address);
		void write_address_mapper(uint16_t address, uint8_t value);

		int m1_prg_bank_offset(int index);
		int m1_chr_bank_offset(int index);
		void m1_update_offset();

		uint8_t *ram;
		uint8_t *sram;
	private:
		NES *nes;
		bool *buttons_p1;
		uint8_t index_p1;
		uint8_t strobe_p1;
		bool *buttons_p2;
		uint8_t index_p2;
		uint8_t strobe_p2;
		bool mirror_prg;

		uint8_t m1_shift_reg;
		uint8_t m1_ctrl;
		uint8_t m1_prg_mode;
		uint8_t m1_chr_mode;
		uint8_t m1_prg_bank;
		uint8_t m1_chr_bank0;
		uint8_t m1_chr_bank1;
		int m1_prg_offsets[2];
		int m1_chr_offsets[2];
		int m2_prg_bank1;
		int m2_prg_bank2;
		int m2_prg_banks;
};
