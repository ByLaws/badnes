import numpy as np

class Memory:
    def __init__(self):
        #0x0 - 0x100: zero page 
        #0x100 - 0x1ff: stack
        #0x200 - 0x800: normal ram
        #0x801 - 0x2000: mirrors 0x0-0x7ff
        #0x2000 - 0x4000: ppu stuff
        #0x4000 - 0x4020: dma stuff
        #0x4020 - 0x6000: expansion rom
        #0x6000 - 0x8000: sram
        #0x8000+: game pak data
        self.data = np.zeros(2**15, dtype=np.uint8)
        self.program = 0
        self.mirror_prg = False

    def reset(self, program):
        #Miss stack
        j = False
        for i in range(4,0x2000,4):
            j = not j
            self.data[i:i+4] = int(j) * 255
        self.program = program
        if (self.program.prg_rom_size <= 16384):
            self.mirror_prg = True

    def stackPush(self, SP, value):
        self.data[0x100 + SP] = value & 0b11111111

        if (SP == 0): #Overflow
            return 0xFF

        return (SP - 1) & 0b11111111

    def stackPull(self, SP):
        if (SP == 0xff): #Overflow
            SP = 0x0
        else:
            SP +=  1

        value = self.data[0x100 + SP] & 0b11111111

        return SP & 0b11111111, value & 0b11111111

    def writeAddress(self, address, value):
        if (address < 0x8000):
            if (address >= 0x2000 and address < 0x2008 or address == 0x4014):
                return ppu.ppu_mem.write(address, value)
            self.data[address] = value
            return;

        print("UNDEFINED WRITE ADDRESS:", address)

    def readAddress16Bug(self, address):
        a = address
        b = (address & 0xFF00) | ((address + 1) & 0xFF)
        return ((self.readAddress(b) << 8) | self.readAddress(a)) & 0xffff

    def readAddress16(self, address):
        #All non game pak stuff:
        if (address < 0x8000):
           return  (self.data[address]) | (self.data[address+1] << 8)

        #PRG ROM is mirrored at 0xc000 if 16kb
        if (address >= 0xc000 and self.mirror_prg):
            address -= 16384

        #Subtract PRG ROM offset
        address -= 0x8000

        return  (self.program.prg[address]) | (self.program.prg[address+1] << 8)

    def readAddress(self, address):
        #All non game pak stuff:
        if (address < 0x8000):
            if (address >= 0x2000 and address < 0x2008 or address == 0x4014):
                return ppu.ppu_mem.read(address)
            return self.data[address]

        #Game pak stuff:

        #PRG ROM is mirrored at 0xc000 if 16kb
        if (address >= 0xc000 and self.mirror_prg):
            address -= 16384

        #Subtract PRG ROM offset
        address -= 0x8000

        return self.program.prg[address]

memory = Memory()

import local_ppu as ppu
