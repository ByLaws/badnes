import numpy as np
import local_cpu as cpu
from PIL import Image

palette_conv = ((70,70,70),(0,6,90),(0,6,120),(2,6,115),(53,3,76),(87,0,14),(90,0,0),(65,0,0),(18,2,0),(0,20,0),(0,30,0),(0,21,33),(0,0,0),(0,0,0),(0,0,0),
                (157, 157, 157), (0, 74, 185), (5, 48, 225), (87, 24, 218), (159, 7, 167), (204, 2, 85), (207, 11, 0), (164, 35, 0), (92, 63, 0), (11, 88, 0), (0, 102, 0), (0, 103, 19), (0, 94, 110), (0, 0, 0), (0, 0, 0), (0, 0, 0),
                (254, 255, 255),(31, 158, 255),(83, 118, 255),(152, 101, 255),(252, 103, 255),(255, 108, 179),(255, 116, 102),(255, 128, 20),(196, 154, 0),(113, 179, 0),(40, 196, 33),(0, 200, 116),(0, 191, 208),(43, 43, 43),(0, 0, 0),(0, 0, 0),
                (254, 255, 255),(158, 213, 255),(175, 192, 255),(208, 184, 255),(254, 191, 255),(255, 192, 224),(255, 195, 189),(255, 202, 156),(231, 213, 139),(197, 223, 142),(166, 230, 163),(148, 232, 197),(146, 228, 235),(167, 167, 167),(0, 0, 0),(0, 0, 0)
                )
class PPUMemory:
    def __init__(self):
        self.vram = np.zeros(0x8000, dtype=np.uint8)
        self.sprite = np.zeros(0x100, dtype=np.uint8)
        self.palette = np.zeros(0x20, dtype=np.uint8)
        self.ctrl_nmi_enable = 0
        self.ctrl_sprite_size = 0
        self.ctrl_bg_pattern_address = 0
        self.ctrl_sprite_pattern_address = 0
        self.ctrl_vram_increment = 0
        self.ctrl_nametable_base = 0
        self.mask_greyscale = 0
        self.mask_bg_leftmost = 0
        self.mask_sprites_leftmost = 0
        self.mask_sprites = 0
        self.mask_bg = 0
        self.mask_emph_r = 0
        self.mask_emph_g = 0
        self.mask_emph_b = 0
        self.stat_overflow = 0
        self.stat_sprite_zero_hit = 0
        self.stat_in_vblank = 0
        self.scroll_reg = 0
        self.latch_reg = 0
        self.odm_addr_reg = 0
        self.vram_addr_reg = 0
        self.odd_frame = 0
        self.frame = 0
        self.scanline = 0
        self.tile_data = 0
        self.cycle = 0
        self.nmi_delay = 0
        self.nmi_occured = 0
        self.nmi_previous = 0
        self.attribute_table_byte = 0
        self.nametable_byte = 0
        self.high_tile_byte = 0
        self.low_tile_byte = 0
        self.inte = 0
        self.image = np.zeros( (300,300, 3), dtype=np.uint8)
        self.x = 0
        self.w = 0
        self.t = 0
        self.buffer = 0

    def write_control_flags(self, value):
        self.ctrl_nametable_base = value & 0b11
        self.ctrl_vram_increment = (value >> 2) & 0b1
        self.ctrl_sprite_pattern_address = (value >> 3) & 0b1
        self.ctrl_bg_pattern_address = (value >> 4) & 0b1
        self.ctrl_sprite_size = (value >> 5) & 0b1
        self.ctrl_nmi_enable = (value >> 7) & 0b1
        self.nmi_update()
        self.t = (self.t & 0xF3FF) | ((((value) & 0x03) << 10) & 0xffff)
    def write_mask_flags(self, value):
        self.mask_greyscale = value & 0b1
        self.mask_bg_leftmost = (value >> 1) & 0b1
        self.mask_sprites_leftmost = (value >> 2) & 0b1
        self.mask_sprites = (value >> 3) & 0b1
        self.mask_bg = (value >> 4) & 0b1
        self.mask_emph_r = (value >> 5) & 0b1
        self.mask_emph_g = (value >> 6) & 0b1
        self.mask_emph_b = (value >> 7) & 0b1
    
    def read_status_flags(self):
        value = self.latch_reg & 0b11111
        value |= (self.stat_overflow & 0b1) << 5
        value |= (self.stat_sprite_zero_hit & 0b1) << 6
        if (self.nmi_occured):
            value |= 1 << 7
        self.nmi_occured = False
        self.nmi_update()

        self.w = 0

        return value

    def mirror_address(self, address):
        if ((address & 0xff00) == 0x3f00):
            address &= 0xff1f

        elif ((address & 0xfff3) == 0x3f10):
                address &= 0x3f0f
        elif ((address & 0xf000) == 0x2000):
            if (self.program.mirroring):
                address &= 0xf7ff #vertical
            else:
                address &= 0xfbff #horizontal
        elif (address >= 0x3000 and address < 0x3f00):
            address -= 0x1000
        elif (address < 0x4000):
            if (address >= 16 and address%4 == 0):
                address -= 16

        return address
    def read_direct(self, address):
        vram_address = self.mirror_address(address)
        if (vram_address < 0x2000):
#            print("CHR READ", vram_address)
            return self.program.chr[vram_address] & 0b11111111
        elif (vram_address < 0x3f00):
            return self.vram[vram_address - 0x2000]
        elif (vram_address < 0x4000):
            return self.palette[vram_address - 0x3f00]


    def read(self, address):
#        print("Read ppu addr:", address)
        if (address == 0x2002):
            ret = self.read_status_flags() & 0xff
#           print("Got value:", ret)
            return ret
        elif (address == 0x2004):
            ret = self.sprite[odm_addr_reg & 0xff] & 0xff
#          print("Got value:", ret)
        elif (address == 0x2007):
#            print("Address is vram")
            vram_address = self.mirror_address(self.vram_addr_reg)
            if (vram_address < 0x2000):
                ret = self.buffer
                self.buffer = self.program.chr[vram_address] & 0b11111111
            elif (vram_address < 0x3f00):
                ret = self.buffer
                self.buffer = self.vram[vram_address - 0x2000]
            elif (vram_address < 0x4000):
                ret = self.palette[vram_address - 0x3f00]

#            print("Got value:", ret)
            if (self.ctrl_vram_increment):
                self.vram_addr_reg += 32
            else:
                self.vram_addr_reg += 1
        else:
            return self.latch_reg & 0xff
        return ret
    
    def write(self, address, value):
#        print("Write", value, "to ppu addr:", address)
        self.latch_reg = value & 0b11111111
        if (address == 0x2000):
            self.write_control_flags(value)
            return
        elif (address == 0x2001):
            self.write_mask_flags(value)
            return
        elif (address == 0x2003):
            self.oam_addr_reg  = value & 0xff
            return
        elif (address == 0x2004):
            self.sprite[self.oam_addr_reg] = value & 0xff
            self.oam_addr_reg = (self.oam_addr_reg + 1) & 0xff
        elif (address == 0x2005):
            if (self.w == 0):
                self.t = (self.t & 0xFFE0) | (((value & 0xff) >> 3) & 0xff)
                self.t &= 0xffff
                self.x = value & 0x07
                self.w = 1
            else:
                self.t = (self.t & 0x8FFF) | ((((value & 0xff) & 0x07) << 12) & 0xffff)
                self.t &= 0xffff
                self.t = (self.t & 0xFC1F) | ((((value & 0xff) & 0xF8) << 2) & 0xffff)
                self.t &= 0xffff
                self.w = 0
            return
        elif (address == 0x2006):
            if (self.w == 0):
                self.t = (self.t & 0x80FF) | ((((value & 0xff) & 0x3F) << 8) & 0xffff)
                self.t &= 0xffff
                self.w = 1
            else:
                self.t = (self.t & 0xFF00) | (value & 0xff)
                self.vram_addr_reg = self.t
                self.w = 0
            return
        elif (address == 0x2007):
#            print("Write is vram")
            vram_address = self.mirror_address(self.vram_addr_reg)
            if (vram_address < 0x2000):
#                print("CHR WRITE", vram_address)
                self.program.chr[vram_address] = value & 0b11111111
            elif (vram_address < 0x3f00):
                self.vram[vram_address - 0x2000] = value & 0b11111111
            elif (vram_address < 0x4000):
                self.palette[vram_address - 0x3f00] = value & 0b11111111

            if (self.ctrl_vram_increment):
                self.vram_addr_reg += 32
            else:
                self.vram_addr_reg += 1
        elif (address == 0x4014):
            page = (value & 0b11111111) << 8
            for i in range(256):
                self.sprite[self.oam_addr_reg] = memory.data[page + i]
                self.oam_addr_reg = (self.oam_addr_reg + 1) & 0xff
    def nmi_update(self):
        nmi = int(self.nmi_occured and self.ctrl_nmi_enable)
        if (nmi and not self.nmi_previous):
            self.nmi_delay = 15
        self.nmi_previous = nmi
    def reset(self, prog):
        self.program = prog


ppu_mem = PPUMemory()
def reset(program):
    ppu_mem.reset(program)

def bg_pixel():
    #s/0/(7-ppu.x) * 4
    return (((ppu_mem.tile_data >> 32) & 0xffffffff) >> ((7-0)*4)) & 0x0f
def draw_new_pixel():
    x = ppu_mem.cycle - 1
    y = ppu_mem.scanline

    if (ppu_mem.mask_bg):
        background = bg_pixel()
    else:
        background = 0

    if (x < 8):
        if (ppu_mem.mask_bg_leftmost):
            background = 0

    b = not (background%4 == 0)

    color = background

    if (color >= 16 and color%4 == 0):
        color -= 16
    
    if (color):
        print("YAY color:", color)
    ppu_mem.image[y, x] = list(palette_conv[ppu_mem.palette[color]])




def cycle():
    ppu_mem.inte += 1
#    print("Start PPU:")
    print("ppu->scanline", ppu_mem.scanline)
    print("ppu->cycle", ppu_mem.cycle)
    print("vram_addr_reg:", ppu_mem.vram_addr_reg)
#    print("ppu_mem.frame", ppu_mem.frame)
#    print("ppu_mem.odd_frame", ppu_mem.odd_frame)
#    print("ppu_mem.vram_addr_reg", ppu_mem.vram_addr_reg)
    if (ppu_mem.nmi_delay):
#        print("nmi delay", ppu_mem.nmi_delay)
        ppu_mem.nmi_delay -= 1
        if (ppu_mem.nmi_delay == 0 and ppu_mem.ctrl_nmi_enable and ppu_mem.nmi_occured):
#            print("fire nmi")
            cpu.nmi()

    start_frame = False
    if (ppu_mem.mask_bg or ppu_mem.mask_sprites):
        if (ppu_mem.scanline == 261 and ppu_mem.cycle == 339):
            ppu_mem.cycle = 0
            ppu_mem.scanline = 0
            ppu_mem.frame += 1
            ppu_mem.odd_frame ^= 1
            start_frame = True

    if (not start_frame):
        ppu_mem.cycle += 1
        if (ppu_mem.cycle > 340): #Next row
            ppu_mem.cycle = 0
            ppu_mem.scanline += 1
            if (ppu_mem.scanline > 261):
                ppu_mem.cycle = 0
                ppu_mem.scanline = 0
                ppu_mem.frame += 1
                ppu_mem.odd_frame ^= 1

    if (ppu_mem.mask_bg or ppu_mem.mask_sprites): #If drawing anything
        if (ppu_mem.scanline < 240 and ppu_mem.cycle > 0 and ppu_mem.cycle <= 256):
            draw_new_pixel()

        if ((ppu_mem.scanline == 261 or ppu_mem.scanline < 240) and ((ppu_mem.cycle >= 321 and ppu_mem.cycle <= 336) or (ppu_mem.cycle >= 1 and ppu_mem.cycle <=256))):
            ppu_mem.tile_data = (ppu_mem.tile_data << 4) & 0xffffffffffffffff
            state = ppu_mem.cycle % 8
#            print("state:", state)
            if (state == 1):
                ppu_mem.nametable_byte = ppu_mem.read_direct(0x2000 | (ppu_mem.vram_addr_reg & 0x0fff))
            elif (state == 3):
                address = 0x23C0 | (ppu_mem.vram_addr_reg & 0x0C00) | ((ppu_mem.vram_addr_reg >> 4) & 0x38) | ((ppu_mem.vram_addr_reg >> 2) & 0x07)
                shift = ((ppu_mem.vram_addr_reg >> 4) & 4) | (ppu_mem.vram_addr_reg & 2)
                ppu_mem.attribute_table_byte = (((ppu_mem.read_direct(address) >> shift) & 3) << 2) & 0xff
            elif (state == 5):
                fine_y = (ppu_mem.vram_addr_reg >> 12) & 7
                address = 0x1000 * ppu_mem.ctrl_bg_pattern_address + ppu_mem.nametable_byte*16 + fine_y
                print("fine_y:", fine_y, "address:", address)
                ppu_mem.low_tile_byte = ppu_mem.program.chr[address & 0xffff]
            elif (state == 7):
                fine_y = (ppu_mem.vram_addr_reg >> 12) & 7
                address = 0x1000 * ppu_mem.ctrl_bg_pattern_address + ppu_mem.nametable_byte*16 + fine_y
                ppu_mem.high_tile_byte = ppu_mem.program.chr[(address + 8) & 0xffff]
            elif (state == 0):
                tmp_data = 0
                print("attribute_byte:", ppu_mem.attribute_table_byte)
                print("low_tile_byte:", ppu_mem.low_tile_byte)
                print("high_tile_byte:", ppu_mem.high_tile_byte)
                for i in range(8):
                    tmp_data = int((tmp_data << 4)) & 0xffffffffffffffff
                    tmp_data |= int(ppu_mem.attribute_table_byte | ((ppu_mem.low_tile_byte & 0x80) >> 7) | ((ppu_mem.high_tile_byte & 0x80) >> 6)) & 0xffffffff
                    ppu_mem.low_tile_byte = (ppu_mem.low_tile_byte << 1) & 0xff
                    ppu_mem.high_tile_byte = (ppu_mem.high_tile_byte << 1) & 0xff
                ppu_mem.tile_data |= int(tmp_data)
                ppu_mem.tile_data &= 0xffffffffffffffff
                print("tile_data:", ppu_mem.tile_data)

        if (ppu_mem.scanline == 261 and ppu_mem.cycle >= 280 and ppu_mem.cycle <= 304):
            ppu_mem.vram_addr_reg = (ppu_mem.vram_addr_reg & 0x841f) | (ppu_mem.t & 0x041f)
            print("t:", ppu_mem.t)
        if (ppu_mem.scanline < 240 or ppu_mem.scanline == 261):
            if (ppu_mem.cycle == 256):
                if (ppu_mem.vram_addr_reg & 0x7000 == 0x7000):
                    ppu_mem.vram_addr_reg &= 0x8fff
                    y = (ppu_mem.vram_addr_reg & 0x03e0) >> 5

                    if (y == 29): #Switch nametable
                        y = 0
                        ppu_mem.vram_addr_reg ^= 0x800
                    elif (y == 31):
                        y = 0
                    else:
                        y += 1
                    print("y:", y)
                    ppu_mem.vram_addr_reg = (ppu_mem.vram_addr_reg & 0xfc1f) | (y << 5)
                else:
                    ppu_mem.vram_addr_reg += 0x1000
                    print("add 0x1000")
            elif (ppu_mem.cycle == 257):
                ppu_mem.vram_addr_reg = (ppu_mem.vram_addr_reg & 0xFBE0) | (ppu_mem.t & 0x041F)
            elif ((ppu_mem.cycle >= 321 and ppu_mem.cycle <= 336) or (ppu_mem.cycle >= 1 and ppu_mem.cycle <= 256)):
                if (ppu_mem.cycle%8 == 0):
                    print("AP:", ppu_mem.vram_addr_reg & 0x001f)
                    if (ppu_mem.vram_addr_reg & 0x001f == 31):
                        print("was:", ppu_mem.vram_addr_reg)
                        ppu_mem.vram_addr_reg &= 0xffe0
                        ppu_mem.vram_addr_reg ^= 0x0400
                        print("now:", ppu_mem.vram_addr_reg)
                    else:
                        ppu_mem.vram_addr_reg += 1
                        print("add 1")
    if (ppu_mem.scanline == 241 and ppu_mem.cycle == 1):
        ppu_mem.nmi_occured = 1
        ppu_mem.stat_in_vblank = 1
        ppu_mem.nmi_update()
        img = Image.fromarray(ppu_mem.image)
        img.save("img/" +str(ppu_mem.inte) + ".bmp")
        ppu_mem.image = np.zeros( (300,300, 3), dtype=np.uint8)


    if (ppu_mem.cycle == 1 and ppu_mem.scanline == 261):
        ppu_mem.nmi_occured = 0
        ppu_mem.nmi_update()
        ppu_mem.stat_in_vblank = 0
        ppu_mem.stat_sprite_zero_hit = 0
        ppu_mem.stat_overflow = 0




    

from local_memory import memory


