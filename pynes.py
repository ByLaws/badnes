import local_cpu as cpu
import local_ppu as ppu
import local_program as program
from local_memory import memory
import cProfile
class Nes:
    def load_program(self, rom):
        program.load(rom)
    
    def cycle(self):
        cycles = cpu.cycle()
        for i in range(cycles * 3):
            ppu.cycle()
    
    def reset(self):
        memory.reset(program.prog)
        ppu.reset(program.prog)
        cpu.reset()

system = Nes()
system.load_program("test.nes")
system.reset()
#cycles = 0
'''
data = 0
for i in range(33):
    stri = ""
    low =program.prog.chr[0x1000+i]
    high =program.prog.chr[0x1000+i+8]
    for j in range(8):
        data = (low & 0x80) >> 7
        data |= (high & 0x80) >> 6
        stri = stri + str(data)
        high <<= 1
        low <<= 1
    print(stri)
'''
while(True): 
    a= input()
    if (a[0] == '+'):
        b = int(a[1:len(a)])
        cProfile.run('for i in range(b): system.cycle()')
    elif (a[0] == 'p'):
        print("palette dump:")
        print(ppu.ppu_mem.palette)
    else:
        b = int(a, 16);
        while not (cpu.registers.PC == b):
            system.cycle()
#while(cycles < 29781): cycles += system.cycle()
