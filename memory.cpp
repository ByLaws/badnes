#include <iostream>
#include "memory.h"
#include "ppu.h"
#include "nes.h"

Memory::Memory(NES *nes) {
	this->nes = nes;
	this->index_p1 = 0;
	this->ram = new uint8_t[0x10000];
	this->sram = new uint8_t[0x2000];
	this->index_p2 = 0;
	this->strobe_p1 = 0;
	this->strobe_p2= 0;
	this->m1_shift_reg = 0x10;
}

void Memory::reset() {
	bool tmp = false;
	for (int i = 4; i < 0x2000; i += 4) {
		tmp = !tmp;
		if (i >= 0x100 && i < 0x200) {
			this->ram[i] = 0xFF;
			this->ram[i+1] = 0xFF;
			this->ram[i+2] = 0xFF;
			this->ram[i+3] = 0xFF;
		} else {
			this->ram[i] = tmp ? 255 : 0;
			this->ram[i+1] = tmp ? 255 : 0;
			this->ram[i+2] = tmp ? 255 : 0;
			this->ram[i+3] = tmp ? 255 : 0;
		}
	}
	if (this->nes->program()->prg_rom_size <= 16384) {
		this->mirror_prg = true;
		std::cout << "Mirroring PRG ROM" << std::endl;
	}
	if (this->nes->program()->mapper == 2) {
		this->m2_prg_banks = this->nes->program()->prg_rom_size / 0x4000;
		std::cout << "Mapper 2: has " << this->m2_prg_banks << " banks"<< std::endl;
		this->m2_prg_bank1 = 0;
		this->m2_prg_bank2 = this->m2_prg_banks - 1;
	}
	if (this->nes->program()->mapper == 1) {
		this->m1_prg_offsets[1] = m1_prg_bank_offset(-1);
		this->m1_prg_mode = 0;
		this->m1_chr_mode = 0;
		this->m1_ctrl = 0;
		this->m1_prg_bank = 0;
		this->m1_chr_bank0 = 0;
		this->m1_chr_bank1 = 0;
	}
}



void Memory::stack_push(uint8_t &SP, uint8_t value) {
	this->ram[0x100 + SP] = value;

	if (SP == 0)
		SP = 0xff;
	else
		SP--;
}

uint8_t Memory::stack_pull(uint8_t &SP) {
	if (SP == 0xff)
		SP = 0;
	else
		SP++;

	return this->ram[0x100 + SP];
}

void Memory::write_address(uint16_t address, uint8_t value) {
	if (address >= 0x6000) {
		write_address_mapper(address, value);
		return;
	}

	if ((address >= 0x2000 && address < 0x2008) || address == 0x4014) {
		this->nes->ppu()->write(address, value);
		return;
	}

	if (address == 0x4016) {
		this->strobe_p1 = value;
		this->strobe_p2 = value;
		if (value & 0b1) {
			this->index_p1 = 0;
			this->index_p2 = 0;
		}
		return;
	}

	this->ram[address] = value;
}

uint16_t Memory::read_address16(uint16_t address) {
	if (address < 0x6000) {
		if ((address >= 0x2000 && address < 0x2008) || address == 0x4014)
			return (uint16_t)this->nes->ppu()->read(address) | ((uint16_t)this->nes->ppu()->read(address+1) << 8);

		return (uint16_t)this->ram[address] | ((uint16_t)this->ram[address+1] << 8);
	}

	return ((uint16_t)read_address_mapper(address) | (uint16_t)((uint16_t)read_address_mapper(address+1) << 8));
}

uint16_t Memory::read_address16_bug(uint16_t address) {
	return ((uint16_t)read_address((address & 0xFF00) | ((address + 1) & 0xFF)) << 8) | (uint16_t)read_address(address);
}

uint8_t Memory::read_address(uint16_t address) {
	if (address < 0x6000) {
		if ((address >= 0x2000 && address < 0x2008) || address == 0x4014)
			return this->nes->ppu()->read(address);

		if (address == 0x4016) {
			uint8_t ret = 0; 

			if (this->index_p1 < 8 && this->buttons_p1[this->index_p1])
				ret = 1;

			this->index_p1++;

			if (this->strobe_p1 & 1)
				this->index_p1 = 0;

			return ret;
		}
		if (address == 0x4017) {
			uint8_t ret = 0; 

			if (this->index_p2 < 8 && this->buttons_p2[this->index_p2])
				ret = 1;

			this->index_p2++;

			if (this->strobe_p2 & 1)
				this->index_p2 = 0;

			return ret;
		}
		return this->ram[address];
	}
	return read_address_mapper(address);
}

uint8_t Memory::read_address_mapper(uint16_t address) {
	if (address < 0x2000 && this->nes->program()->mapper != 1)
		return this->nes->program()->chr[address];

	if (this->nes->program()->mapper == 0) {
		if (this->mirror_prg && address >= 0xc000)
			address -= 16384;

		address -= 0x8000;

		return this->nes->program()->prg[address];
	}

	if (this->nes->program()->mapper == 2) {
		if (address >= 0xc000) {
			return this->nes->program()->prg[this->m2_prg_bank2 * 0x4000 + ((int)address - 0xc000)];
		}
		if (address >= 0x8000) {
			return this->nes->program()->prg[this->m2_prg_bank1 * 0x4000 + ((int)address - 0x8000)];
		}
		if (address >= 0x6000) {
			return this->sram[((int)address - 0x6000)];
		}
	}
	if (this->nes->program()->mapper == 1) {
		if (address < 0x2000) {
			return this->nes->program()->chr[this->m1_chr_offsets[address/0x1000] + (int)((int)address % 0x1000)];
		}
		if (address >= 0x8000) {
			address -= 0x8000;
			return this->nes->program()->prg[this->m1_prg_offsets[address/0x4000] + (int)((int)address % 0x4000)];
		}
		if (address >= 0x6000) {
			return this->sram[((int)address - 0x6000)];
		}
	}
	return 0;
}

void Memory::write_address_mapper(uint16_t address, uint8_t value) {
	if (address < 0x2000 && this->nes->program()->mapper != 1) {
		this->nes->program()->chr[address] = value;
		return;
	}

	if (this->nes->program()->mapper == 0) {
		if (this->mirror_prg && address >= 0xc000)
			address -= 16384;

		address -= 0x8000;

		this->nes->program()->prg[address] = value;
		return;
	}

	if (this->nes->program()->mapper == 2) {
		if (address >= 0x8000) {
			this->m2_prg_bank1 = (int)value % this->m2_prg_banks;
			return;
		}
		if (address >= 0x6000) {
			this->sram[((int)address - 0x6000)] = value;
			return;
		}
	}

	if (this->nes->program()->mapper == 1) {
		if (address < 0x2000) {
			this->nes->program()->chr[this->m1_chr_offsets[address/0x1000] + (int)((int)address % 0x1000)] = value;
			return;
		}
		if (address >= 0x8000) {
			if (value & (uint8_t)0x80) {
				this->m1_shift_reg = 0x10;
				this->m1_ctrl = this->m1_ctrl | 0x0c;
				this->m1_chr_mode = ((this->m1_ctrl) >> 4) & 1;
				this->m1_prg_mode = ((this->m1_ctrl) >> 2) & 3;

//				if ((this->m1_ctrl | 0x0c) & (uint8_t)3 == (uint8_t)0)
//					this->nes->program->mirroring = MIRROR_SINGLE_0;
//				else if ((this->m1_ctrl | 0x0c) & (uint8_t)3 == (uint8_t)1)
//					this->nes->program->mirroring = MIRROR_SINGLE_1;
//				else if ((this->m1_ctrl | 0x0c) & (uint8_t)3 == (uint8_t)2)
//					this->nes->program->mirroring = MIRROR_SINGLE_VERTICAL;
//				else
//					this->nes->program->mirroring = MIRROR_SINGLE_HORIZONTAL;

				m1_update_offset();
				return;
			} else {
				bool complete = this->m1_shift_reg & 1;
				this->m1_shift_reg >>= 1;
				this->m1_shift_reg |= (value & 1) << 4;

				if (complete) {
					if (address <= 0x9fff) {
						this->m1_ctrl = this->m1_shift_reg;
						this->m1_chr_mode = ((this->m1_ctrl | 0x0c) >> 4) & 1;
						this->m1_prg_mode = ((this->m1_ctrl | 0x0c) >> 2) & 3;

//						if ((this->m1_ctrl | 0x0c) & (uint8_t)3 == (uint8_t)0)
//							this->nes->program()->mirroring = MIRROR_SINGLE_0;
//						else if ((this->m1_ctrl | 0x0c) & (uint8_t)3 == (uint8_t)1)
//							this->nes->program()->mirroring = MIRROR_SINGLE_1;
//						else if ((this->m1_ctrl | 0x0c) & (uint8_t)3 == (uint8_t)2)
//							this->nes->program()->mirroring = MIRROR_SINGLE_VERTICAL;
//						else
//							this->nes->program()->mirroring = MIRROR_SINGLE_HORIZONTAL;

						m1_update_offset();
						return;
					} else if (address <= 0xbfff) {
						this->m1_chr_bank0 = this->m1_shift_reg;
						m1_update_offset();
						return;
					} else if (address <= 0xdfff) {
						this->m1_chr_bank1 = this->m1_shift_reg;
						m1_update_offset();
						return;
					} else if (address <= 0xffff) {
						this->m1_prg_bank = this->m1_shift_reg & 0xf;
						m1_update_offset();
						return;
					}
					this->m1_shift_reg = 0x10;
				}
			}
		}
		if (address >= 0x6000) {
			this->sram[((int)address - 0x6000)] = value;
			return;
		}
	}
}
int Memory::m1_chr_bank_offset(int index) {
	if (index >= 0x80) {
		index -= 0x100;
	}
	index %= this->nes->program()->chr_rom_size / 0x1000;
	int offset = index * 0x1000;
	if (offset < 0) {
		offset += this->nes->program()->chr_rom_size;
	}
	return offset;
}
int Memory::m1_prg_bank_offset(int index) {
	if (index >= 0x80) {
		index -= 0x100;
	}
	index %= this->nes->program()->prg_rom_size / 0x4000;
	int offset = index * 0x4000;
	if (offset < 0) {
		offset += this->nes->program()->prg_rom_size;
	}
	return offset;
}

void Memory::m1_update_offset() {
	if (this->m1_prg_mode == 0 || this->m1_prg_mode == 1) {
		this->m1_prg_offsets[0] = m1_prg_bank_offset((int)(this->m1_prg_bank & 0xFE));
		this->m1_prg_offsets[1] = m1_prg_bank_offset((int)(this->m1_prg_bank | 0x01));
	} else if (this->m1_prg_mode == 2) {
		this->m1_prg_offsets[0] = 0;
		this->m1_prg_offsets[1] = m1_prg_bank_offset((int)(this->m1_prg_bank));
	} else if (this->m1_prg_mode == 3) {
		this->m1_prg_offsets[0] = m1_prg_bank_offset((int)(this->m1_prg_bank));
		this->m1_prg_offsets[1] = m1_prg_bank_offset(-1);
	}
	if (this->m1_chr_mode == 0) {
		this->m1_chr_offsets[0] = m1_chr_bank_offset((int)(this->m1_chr_bank0 & 0xFE));
		this->m1_chr_offsets[1] = m1_chr_bank_offset((int)(this->m1_chr_bank0 | 0x01));
	} else if (this->m1_chr_mode == 1) {
		this->m1_chr_offsets[0] = m1_chr_bank_offset((int)(this->m1_chr_bank0));
		this->m1_chr_offsets[1] = m1_chr_bank_offset((int)(this->m1_chr_bank1));
	}
}
