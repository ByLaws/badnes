import numpy as np

class Program:
    def __init__(self, program):
        self.program = program
        self.prg_rom_size = 16384 * program[4]
        self.chr_rom_size = 8192 * program[5]
        self.mirroring = program[6] & 0b00000001
        self.has_prg_ram = program[6] & 0b0000010
        self.has_trainer = program[6] & 0b0000100
        self.ignore_mirroring = program[6] & 0b0001000
        self.mapper = (program[7] & 0b11110000) | (program[6] >> 4)

        print("PRG ROM size:", self.prg_rom_size)
        print("CHR ROM size:", self.chr_rom_size)
        print("Mirroring (0: horizontal, 1: vertical):", self.mirroring)
        print("PRG RAM (0: no PRG RAM, 1: has PRG RAM):", self.has_prg_ram)
        print("Trainer (0: no trainer, 1: has trainer):", self.has_trainer)
        print("Ignore mirroring (0: use mirroring, 1: use four screen VRAM):", self.ignore_mirroring)
        print("Mapper number:", self.mapper)

        data_start = 16
        if (self.has_trainer):
            data_start += 512

        self.prg = self.program[data_start:data_start + self.prg_rom_size]
        self.chr = self.program[data_start + self.prg_rom_size:data_start + self.prg_rom_size + self.chr_rom_size]

prog = 0
def load(path):
    global prog
    prog = Program(np.fromfile(path, dtype=np.uint8))
    
