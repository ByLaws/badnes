#include "nes.h"
#include <unistd.h>
#include <iostream>
#include <SDL2/SDL.h>
#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#include <dirent.h>
EM_JS(void, print_files, (const char* str), {
	alert(UTF8ToString(str));
});
#endif

SDL_Renderer *renderer;
bool KEYS_p1[8];
bool KEYS_p2[8];
NES nes= NES();

void *pixels;
Uint8 *base;
#ifdef __EMSCRIPTEN__
SDL_Texture *texture = NULL;
int pitch;
void loops(){for(int i =0;i<10000;i++){nes.cycle();}}
#endif
void draw_pixel_sdl(int x, int y, int r, int g, int b) {
#ifdef __EMSCRIPTEN__
	base = ((Uint8 *)pixels) + (4 * ((y*2) * 600 + (x*2)));
	base[0] = b;
	base[1] = g;
	base[2] = r;
	base[3] = 0xff;
	base = ((Uint8 *)pixels) + (4 * ((y*2) * 600 + (x*2)+1));
	base[0] = b;
	base[1] = g;
	base[2] = r;
	base[3] = 0xff;
	base = ((Uint8 *)pixels) + (4 * (((y*2)+1) * 600 + (x*2)));
	base[0] = b;
	base[1] = g;
	base[2] = r;
	base[3] = 0xff;
	base = ((Uint8 *)pixels) + (4 * (((y*2)+1) * 600 + (x*2)+1));
	base[0] = b;
	base[1] = g;
	base[2] = r;
	base[3] = 0xff;
#else
	SDL_SetRenderDrawColor(renderer, r, g, b, 0);
	SDL_RenderDrawPoint(renderer, x, y);
#endif
}

void frame_done_sdl() {
	SDL_Event event;
#ifdef __EMSCRIPTEN__
	SDL_UnlockTexture(texture);
	SDL_RenderClear(renderer);
	SDL_RenderCopy(renderer, texture, NULL, NULL);
#else
	SDL_RenderPresent(renderer);
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
	SDL_RenderClear(renderer);
#endif
	if (SDL_PollEvent(&event)) {
		switch( event.type ){
		case SDL_KEYDOWN:
			switch (event.key.keysym.sym){
				case SDLK_d: KEYS_p1[7] = true; break;
				case SDLK_a: KEYS_p1[6] = true; break;
				case SDLK_w: KEYS_p1[4] = true; break;
				case SDLK_s: KEYS_p1[5] = true; break;
				case SDLK_z: KEYS_p1[2] = true; break; // Select
				case SDLK_x: KEYS_p1[3] = true; break; // Start
				case SDLK_q: KEYS_p1[1] = true; break; // B
				case SDLK_e: KEYS_p1[0] = true; break; // A
				case SDLK_k: KEYS_p2[7] = true; break;
				case SDLK_h: KEYS_p2[6] = true; break;
				case SDLK_u: KEYS_p2[4] = true; break;
				case SDLK_j: KEYS_p2[5] = true; break;
				case SDLK_n: KEYS_p2[2] = true; break; // Select
				case SDLK_m: KEYS_p2[3] = true; break; // Start
				case SDLK_y: KEYS_p2[2] = true; break; // B
				case SDLK_i: KEYS_p2[0] = true; break; // A
			}
			break;

		case SDL_KEYUP:
			switch (event.key.keysym.sym){
				case SDLK_d: KEYS_p1[7] = false; break;
				case SDLK_a: KEYS_p1[6] = false; break;
				case SDLK_w: KEYS_p1[4] = false; break;
				case SDLK_s: KEYS_p1[5] = false; break;
				case SDLK_z: KEYS_p1[2] = false; break; // Select
				case SDLK_x: KEYS_p1[3] = false; break; // Start
				case SDLK_q: KEYS_p1[1] = false; break; // B
				case SDLK_e: KEYS_p1[0] = false; break; // A
				case SDLK_k: KEYS_p2[7] = false; break;
				case SDLK_h: KEYS_p2[6] = false; break;
				case SDLK_u: KEYS_p2[4] = false; break;
				case SDLK_j: KEYS_p2[5] = false; break;
				case SDLK_n: KEYS_p2[2] = false; break; // Select
				case SDLK_m: KEYS_p2[3] = false; break; // Start
				case SDLK_y: KEYS_p2[2] = false; break; // B
				case SDLK_i: KEYS_p2[0] = false; break; // A

			}
			break;

		case SDL_QUIT:
			if (event.type == SDL_QUIT)
				abort();
		default:
			break;
		}
	}

	nes.mem()->set_pressed_buttons(false, KEYS_p1);
	nes.mem()->set_pressed_buttons(true, KEYS_p2);

#ifdef __EMSCRIPTEN__
	SDL_LockTexture(texture, NULL, &pixels, &pitch);
#endif
}

int main(int argc, char **argv) {
	SDL_Window *window;

#ifdef __EMSCRIPTEN__
	DIR *dpdf;
	struct dirent *epdf;
	std::string name;
	dpdf = opendir("./");
	if (dpdf != NULL){
		while ((epdf = readdir(dpdf))){
			name += std::string(epdf->d_name) + ", ";
		}
	}

	closedir(dpdf);
	print_files(name.c_str());
	name = "";
	std::cin >> name;
	nes.load(name);
#else
	nes.load(argv[1]);
#endif
	nes.reset();
	nes.mem()->set_pressed_buttons(false, KEYS_p1);
	nes.mem()->set_pressed_buttons(true, KEYS_p2);

	SDL_Init(SDL_INIT_VIDEO);
	SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1");
	SDL_CreateWindowAndRenderer(600, 600, 0, &window, &renderer);
#ifdef __EMSCRIPTEN__
	texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STREAMING,600,600);
	SDL_LockTexture(texture, NULL, &pixels, &pitch);
#endif
	SDL_RenderSetScale(renderer, 2, 2);
	nes.set_render(&draw_pixel_sdl, &frame_done_sdl);
#ifdef __EMSCRIPTEN__
	emscripten_set_main_loop(loops, 0, 1);
#else
	while (1) {
		std::string tsk = "";
		int num  = 0;
		std::cout << "tsk:";
		std::cin >> tsk;
		if (tsk == "peek") {
			std::cin >> num;
			std::cout << "val: " << std::hex << (unsigned int)nes.mem()->read_address((uint16_t)num) << std::endl;
		}
		std::cout << "num:";
		std::cin >> num;
		for (int i = 0; i < num; i++)
			nes.cycle();
	}
#endif
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
	return EXIT_SUCCESS;
}
