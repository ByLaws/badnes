from enum import IntEnum, Enum
import numpy as np
import local_program as prog
from local_memory import memory

class StatusFlags(IntEnum):
    CARRY = 1
    ZERO = 2 ** 1
    IRQ_DISABLE = 2 ** 2
    DECIMAL = 2 ** 3
    BREAK_MODE = 2 ** 4 #Research
    RESERVED = 2 ** 5 #Research
    OVERFLOW = 2 ** 6
    NEGATIVE = 2 ** 7

class AddressingModes(Enum):
    ZERO_PAGE = 0
    INDEXED_ZERO_PAGE_X = 1
    INDEXED_ZERO_PAGE_Y = 2
    ABSOLUTE = 3
    INDEXED_ABSOLUTE_X = 4
    INDEXED_ABSOLUTE_Y = 5
    IMPLIED = 6
    ACCUMULATOR = 7
    IMMEDIATE = 8
    RELATIVE = 9
    INDEXED_INDIRECT = 10
    INDIRECT_INDEXED = 11
    INDIRECT = 12

class Registers:
    def __init__(self):
        self.A = 0  #Accumulator     u8
        self.X = 0  #Index           u8
        self.Y = 0  #Index           u8
        self.PC = 0 #Program Counter u16
        self.S = 0  #Stack Pointer   u8
        self.SP = 0  #Status Register u8
        self.current_instruction = 0  #Not a real reg, used for addressing relative to current instruction
        self.nmi_pending = False
    def reset(self, resetVector):
        self.S = StatusFlags.IRQ_DISABLE | StatusFlags.RESERVED #Power up value
        self.SP = 0xFD #Power up value
        self.PC = resetVector
        self.current_instruction = resetVector

registers = Registers()

def dump_registers():
    print("A :", hex(registers.A & 0b11111111))
    print("X :", hex(registers.X & 0b11111111))
    print("Y :", hex(registers.Y & 0b11111111))
    print("PC:", hex(registers.PC & 0b1111111111111111))
    print("S :", bin(registers.S & 0b11111111))
    print("SP:", hex(registers.SP & 0b11111111))

#Call after memory reset
def reset():
    vector = memory.readAddress16(0xfffc)
    print("Read reset vector:", hex(vector))
    registers.reset(vector)

def get_size(mode):
    if (mode == AddressingModes.IMPLIED):
        return 1
    elif (mode == AddressingModes.ACCUMULATOR):
        return 1
    elif (mode == AddressingModes.IMMEDIATE):
        return 2
    elif (mode == AddressingModes.ABSOLUTE):
        return 3
    elif (mode == AddressingModes.ZERO_PAGE):
        return 2
    elif (mode == AddressingModes.RELATIVE):
        return 2
    elif (mode == AddressingModes.INDEXED_ZERO_PAGE_X):
        return 2
    elif (mode == AddressingModes.INDEXED_ZERO_PAGE_Y):
        return 2
    elif (mode == AddressingModes.INDEXED_ABSOLUTE_X):
        return 3
    elif (mode == AddressingModes.INDEXED_ABSOLUTE_Y):
        return 3
    elif (mode == AddressingModes.INDEXED_INDIRECT):
        return 2
    elif (mode == AddressingModes.INDIRECT_INDEXED):
        return 2
    elif (mode == AddressingModes.INDIRECT):
        return 3
    else:
        print("ADD SIZE FOR MODE:", mode)

def get_instruction_data(mode):
    if (mode == AddressingModes.IMMEDIATE):
        return registers.current_instruction + 1
    elif (mode == AddressingModes.ACCUMULATOR):
        return registers.A
    elif (mode == AddressingModes.ABSOLUTE):
        return memory.readAddress16(registers.current_instruction + 1)
    elif (mode == AddressingModes.ZERO_PAGE):
        return memory.readAddress(registers.current_instruction + 1)
    elif (mode == AddressingModes.RELATIVE):
        address = memory.readAddress(registers.current_instruction + 1)
        if (address < 0x80):
            address += registers.current_instruction
        else:
            address += registers.current_instruction - 256
        return address + 2
    elif (mode == AddressingModes.INDEXED_ZERO_PAGE_X):
        return ((memory.readAddress(registers.current_instruction + 1) & 0xff)+ registers.X) & 0xff
    elif (mode == AddressingModes.INDEXED_ZERO_PAGE_Y):
        return ((memory.readAddress(registers.current_instruction + 1) & 0xff)+ registers.Y) & 0xff
    elif (mode == AddressingModes.INDEXED_ABSOLUTE_X):
        return ((memory.readAddress16(registers.current_instruction + 1) & 0xffff) + registers.X) & 0xffff
    elif (mode == AddressingModes.INDEXED_ABSOLUTE_Y):
        return ((memory.readAddress16(registers.current_instruction + 1) & 0xffff) + registers.Y) & 0xffff
    elif (mode == AddressingModes.INDEXED_INDIRECT):
        address = memory.readAddress(registers.current_instruction + 1)

        address += registers.X &  0b11111111
        address &= 0b11111111
        address = memory.readAddress16Bug(address)
        return address
    elif (mode == AddressingModes.INDIRECT_INDEXED):
        address = memory.readAddress(registers.current_instruction + 1)
        address = memory.readAddress16Bug(address)
        
        address += registers.Y
        address &= 0b1111111111111111
        return address
    elif (mode == AddressingModes.INDIRECT):
        return memory.readAddress16Bug(memory.readAddress(registers.current_instruction + 1))

#Sets appropriate flags for the given byte
def set_flags(byte):
    registers.S = (registers.S & ~StatusFlags.NEGATIVE) | (byte & 0b10000000) #Set negative flag
    registers.S = (registers.S & ~StatusFlags.ZERO) | ((not min(byte, 1)) << 1) #Set zero flag

def instruction_ADC(mode, sbc=False):
    data2 = memory.readAddress(get_instruction_data(mode))
    
    if (sbc):
        data = ~data2 # -data - 1
    else:
        data = data2

    data += (registers.S & StatusFlags.CARRY)
    data += registers.A

    if (data > 0b11111111):
        registers.S |= StatusFlags.CARRY
    else:
        registers.S &= ~StatusFlags.CARRY

    if (sbc):
        if ((registers.A ^ data2) & (registers.A ^ data) & 0b10000000):
            registers.S |= StatusFlags.OVERFLOW
        else:
            registers.S &= ~StatusFlags.OVERFLOW
    else:
        if (~(registers.A ^ data2) & (registers.A ^ data) & 0b10000000):
            registers.S |= StatusFlags.OVERFLOW
        else:
            registers.S &= ~StatusFlags.OVERFLOW


    data &= 0b11111111
    registers.A = data
    set_flags(registers.A & 0b11111111)

    return 0

def instruction_AND(mode):
    data = memory.readAddress(get_instruction_data(mode))
    
    registers.A &= data
    set_flags(registers.A & 0b11111111)

    return 0

def instruction_ASL(mode):
    if (mode == AddressingModes.ACCUMULATOR):
        data = registers.A
    else:
        address = get_instruction_data(mode)
        data = memory.readAddress(address)
    
    if (data & 0b10000000):
        registers.S |= StatusFlags.CARRY
    else:
        registers.S &= ~StatusFlags.CARRY

    data *= 2 # left shift
    data &= 0b11111111
    set_flags(data & 0b11111111)

    if (mode == AddressingModes.ACCUMULATOR):
        registers.A = data
    else:
        memory.writeAddress(address, data)

    return 0

def instruction_BCC(mode):
    if (not (registers.S & StatusFlags.CARRY)):
        registers.PC = get_instruction_data(mode)
        return 1 # Branch succeeded
    
    return 0

def instruction_BCS(mode):
    if (registers.S & StatusFlags.CARRY):
        registers.PC = get_instruction_data(mode)
        return 1 # Branch succeeded
    
    return 0

def instruction_BEQ(mode):
    if (registers.S & StatusFlags.ZERO):
        registers.PC = get_instruction_data(mode)
        return 1 # Branch succeeded
    
    return 0

def instruction_BIT(mode):
    data = memory.readAddress(get_instruction_data(mode))

    if ((registers.A & data) == 0):
        registers.S |= StatusFlags.ZERO
    else:
        registers.S &= ~StatusFlags.ZERO

    if (data & 0b01000000):
        registers.S |= StatusFlags.OVERFLOW
    else:
        registers.S &= ~StatusFlags.OVERFLOW

    if (data & 0b10000000):
        registers.S |= StatusFlags.NEGATIVE
    else:
        registers.S &= ~StatusFlags.NEGATIVE

    return 0

def instruction_BMI(mode):
    if (registers.S & StatusFlags.NEGATIVE):
        registers.PC = get_instruction_data(mode)
        return 1 # Branch succeeded
    
    return 0

def instruction_BNE(mode):
    if (not (registers.S & StatusFlags.ZERO)):
        registers.PC = get_instruction_data(mode)
        return 1 # Branch succeeded
    
    return 0

def instruction_BPL(mode):
    if (not (registers.S & StatusFlags.NEGATIVE)):
        registers.PC = get_instruction_data(mode)
        return 1 # Branch succeeded
    
    return 0

#BRK goes here
def instruction_BRK(mode, nmi=False):
    registers.SP = memory.stackPush(registers.SP, ((registers.PC - 1)& 0xff00) >> 8)
    registers.SP = memory.stackPush(registers.SP, (registers.PC - 1)& 0xff)
    registers.SP = memory.stackPush(registers.SP, registers.S)
 
    if (nmi):
        registers.PC = memory.readAddress16(0xfffa)
    else:
        registers.PC = memory.readAddress16(0xfffe)

    return 0

def instruction_BVC(mode):
    if (not (registers.S & StatusFlags.OVERFLOW)):
        registers.PC = get_instruction_data(mode)
        return 1 # Branch succeeded
    
    return 0

def instruction_BVS(mode):
    if (registers.S & StatusFlags.OVERFLOW):
        registers.PC = get_instruction_data(mode)
        return 1 # Branch succeeded
    
    return 0

def instruction_CLC(mode):
    registers.S &= ~StatusFlags.CARRY
    
    return 0

def instruction_CLD(mode):
    registers.S &= ~StatusFlags.DECIMAL
    
    return 0

def instruction_CLI(mode):
    registers.S &= ~StatusFlags.IRQ_DISABLE
    
    return 0

def instruction_CLV(mode):
    registers.S &= ~StatusFlags.OVERFLOW
    
    return 0

def instruction_CMP(mode):
    data = memory.readAddress(get_instruction_data(mode))
    
    if (registers.A & 0b11111111 >= data & 0b11111111):
        registers.S |= StatusFlags.CARRY
    else:
        registers.S &= ~StatusFlags.CARRY
    
    data = (registers.A & 0b11111111) - (data & 0b11111111) 
    set_flags(data & 0b11111111)

    return 0

def instruction_CPX(mode):
    data = memory.readAddress(get_instruction_data(mode))
    
    if (registers.X & 0b11111111 >= data & 0b11111111):
        registers.S |= StatusFlags.CARRY
    else:
        registers.S &= ~StatusFlags.CARRY
    
    data = (registers.X & 0b11111111) - (data  & 0b11111111)
    set_flags(data & 0b11111111)

    return 0

def instruction_CPY(mode):
    data = memory.readAddress(get_instruction_data(mode))
    
    if (registers.Y & 0b11111111 >= data & 0b11111111):
        registers.S |= StatusFlags.CARRY
    else:
        registers.S &= ~StatusFlags.CARRY
    
    data = (registers.Y & 0b11111111) - (data  & 0b11111111)
    set_flags(data & 0b11111111)

    return 0

def instruction_DEC(mode):
    address = get_instruction_data(mode)
    data = memory.readAddress(address)
    
    data -= 1
    set_flags(data & 0b11111111)

    memory.writeAddress(address, data & 0b11111111)
    return 0

def instruction_DEX(mode):
    registers.X -= 1
    registers.X &= 0b11111111
    set_flags(registers.X & 0b11111111)
    return 0

def instruction_DEY(mode):
    registers.Y -= 1
    registers.Y &= 0b11111111
    set_flags(registers.Y & 0b11111111)
    return 0

def instruction_EOR(mode):
    data = memory.readAddress(get_instruction_data(mode))

    registers.A ^= data & 0b11111111
    set_flags(registers.A & 0b11111111)
    return 0

def instruction_INC(mode):
    address = get_instruction_data(mode)
    data = memory.readAddress(address)
    
    data += 1
    set_flags(data & 0b11111111)

    memory.writeAddress(address, data & 0b11111111)
    return 0

def instruction_INX(mode):
    registers.X += 1
    registers.X &= 0b11111111
    set_flags(registers.X & 0b11111111)
    return 0

def instruction_INY(mode):
    registers.Y += 1
    registers.Y &= 0b11111111
    set_flags(registers.Y & 0b11111111)
    return 0

def instruction_JMP(mode):
    registers.PC = get_instruction_data(mode)
    return 0

def instruction_JSR(mode):
    registers.SP = memory.stackPush(registers.SP, ((registers.PC - 1)& 0xff00) >> 8)
    registers.SP = memory.stackPush(registers.SP, (registers.PC - 1)& 0xff)
    registers.PC = get_instruction_data(mode)
    return 0

def instruction_LDA(mode):
    data = memory.readAddress(get_instruction_data(mode))
    registers.A = data
    set_flags(data & 0b11111111)

    return 0

def instruction_LDX(mode):
    data = memory.readAddress(get_instruction_data(mode))
    
    registers.X = data
    set_flags(data & 0b11111111)

    return 0

def instruction_LDY(mode):
    data = memory.readAddress(get_instruction_data(mode))
    
    registers.Y = data
    set_flags(data & 0b11111111)

    return 0

def instruction_LSR(mode):
    if (mode == AddressingModes.ACCUMULATOR):
        data = registers.A
    else:
        address = get_instruction_data(mode)
        data = memory.readAddress(address)
    
    if (data & 0b00000001):
        registers.S |= StatusFlags.CARRY
    else:
        registers.S &= ~StatusFlags.CARRY

    data = data >> 1 #Right shift
    data &= 0b11111111
    set_flags(data & 0b11111111)

    if (mode == AddressingModes.ACCUMULATOR):
        registers.A = data
    else:
        memory.writeAddress(address, data)

    return 0

def instruction_NOP(mode):
    return 0;

def instruction_ORA(mode):
    data = memory.readAddress(get_instruction_data(mode))
    
    registers.A = ((registers.A & 0b11111111) | (data & 0b11111111)) & 0b11111111
    set_flags(data & 0b11111111)

    return 0

def instruction_PHA(mode):
    registers.SP = memory.stackPush(registers.SP, registers.A)

    return 0

def instruction_PHP(mode):
    registers.SP = memory.stackPush(registers.SP, registers.S | StatusFlags.BREAK_MODE)

    return 0

def instruction_PLA(mode):
    registers.SP, registers.A = memory.stackPull(registers.SP)

    set_flags(registers.A)

    return 0

def instruction_PLP(mode):
    registers.SP, status = memory.stackPull(registers.SP)

    status &= ~StatusFlags.BREAK_MODE
    status &= ~StatusFlags.RESERVED

    status |= registers.S & StatusFlags.BREAK_MODE
    status |= registers.S & StatusFlags.RESERVED

    registers.S = status

    return 0

def instruction_ROL(mode):
    if (mode == AddressingModes.ACCUMULATOR):
        data = registers.A
    else:
        address = get_instruction_data(mode)
        data = memory.readAddress(address)
    
    old_status = registers.S

    if (data & 0b10000000):
        registers.S |= StatusFlags.CARRY
    else:
        registers.S &= ~StatusFlags.CARRY

    data = data << 1 #Left shift
    data &= 0b11111111
    data |= old_status & 0b00000001 #Set data bit 0 old carry flag
    set_flags(data & 0b11111111)

    if (mode == AddressingModes.ACCUMULATOR):
        registers.A = data
    else:
        memory.writeAddress(address, data)

    return 0

def instruction_ROR(mode):
    if (mode == AddressingModes.ACCUMULATOR):
        data = registers.A
    else:
        address = get_instruction_data(mode)
        data = memory.readAddress(address)
    
    old_status = registers.S

    if (data & 0b00000001):
        registers.S |= StatusFlags.CARRY
    else:
        registers.S &= ~StatusFlags.CARRY

    data = data >> 1 #Right shift
    data &= 0b11111111
    data |= (old_status & 0b00000001) <<  7 #Set data bit 7 old carry flag
    set_flags(data & 0b11111111)

    if (mode == AddressingModes.ACCUMULATOR):
        registers.A = data
    else:
        memory.writeAddress(address, data)

    return 0

def instruction_RTI(mode):
    cycles = instruction_PLP(mode) + instruction_RTS(mode)
    registers.PC += 0
    return cycles


def instruction_RTS(mode):
    registers.SP, tmp = memory.stackPull(registers.SP)
    registers.SP, tmp2 = memory.stackPull(registers.SP)
    registers.PC = tmp2 << 8 & 0b1111111100000000
    registers.PC |= tmp & 0b0000000011111111
    registers.PC += 1
    return 0

def instruction_SBC(mode):
    return instruction_ADC(mode, sbc=True)

def instruction_SEC(mode):
    registers.S |= StatusFlags.CARRY
    
    return 0

def instruction_SED(mode):
    registers.S |= StatusFlags.DECIMAL
    
    return 0

def instruction_SEI(mode):
    registers.S |= StatusFlags.IRQ_DISABLE
    
    return 0

def instruction_STA(mode):
    address = get_instruction_data(mode)
    
    memory.writeAddress(address, registers.A)

    return 0

def instruction_STX(mode):
    address = get_instruction_data(mode)
    
    memory.writeAddress(address, registers.X)

    return 0

def instruction_STY(mode):
    address = get_instruction_data(mode)
    
    memory.writeAddress(address, registers.Y)

    return 0

def instruction_TAX(mode):
    registers.X = registers.A
    set_flags(registers.X & 0b11111111)
    
    return 0

def instruction_TAY(mode):
    registers.Y = registers.A
    set_flags(registers.Y & 0b11111111)
    
    return 0

def instruction_TSX(mode):
    registers.X = registers.SP
    set_flags(registers.X & 0b11111111)
    
    return 0

def instruction_TXA(mode):
    registers.A = registers.X
    set_flags(registers.A & 0b11111111)
    
    return 0

def instruction_TXS(mode):
    registers.SP = registers.X
    
    return 0

def instruction_TYA(mode):
    registers.A = registers.Y
    set_flags(registers.A & 0b11111111)
    
    return 0


#FIX ALLL THE TIMINGS
InstructionMap = [
        (0,0),
        (0x01, AddressingModes.INDEXED_INDIRECT, 6, instruction_ORA),
        (0,0),
        (0,0),
        (0,0),
        (0x05, AddressingModes.ZERO_PAGE, 3, instruction_ORA),
        (0x06, AddressingModes.ZERO_PAGE, 5, instruction_ASL),
        (0,0),
        (0x08, AddressingModes.IMPLIED, 3, instruction_PHP),
        (0x09, AddressingModes.IMMEDIATE, 2, instruction_ORA),
        (0x0a, AddressingModes.ACCUMULATOR, 2, instruction_ASL),
        (0,0),
        (0,0),
        (0x0d, AddressingModes.ABSOLUTE, 4, instruction_ORA),
        (0x0e, AddressingModes.ABSOLUTE, 6, instruction_ASL),
        (0,0),
        (0x10, AddressingModes.RELATIVE, 2, instruction_BPL),
        (0x11, AddressingModes.INDIRECT_INDEXED, 5, instruction_ORA),
        (0,0),
        (0,0),
        (0,0),
        (0x15, AddressingModes.INDEXED_ZERO_PAGE_X, 4, instruction_ORA),
        (0x16, AddressingModes.INDEXED_ZERO_PAGE_X, 6, instruction_ASL),
        (0,0),
        (0x18, AddressingModes.IMPLIED, 2, instruction_CLC),
        (0x19, AddressingModes.INDEXED_ABSOLUTE_Y, 4, instruction_ORA),
        (0,0),
        (0,0),
        (0,0),
        (0x1d, AddressingModes.INDEXED_ABSOLUTE_X, 4, instruction_ORA),
        (0x1e, AddressingModes.INDEXED_ABSOLUTE_X, 7, instruction_ASL),
        (0,0),
        (0x20, AddressingModes.ABSOLUTE, 6, instruction_JSR),
        (0x21, AddressingModes.INDEXED_INDIRECT, 6, instruction_AND),
        (0,0),
        (0,0),
        (0x24, AddressingModes.ZERO_PAGE, 3, instruction_BIT),
        (0x25, AddressingModes.ZERO_PAGE, 3, instruction_AND),
        (0x26, AddressingModes.ZERO_PAGE, 5, instruction_ROL),
        (0,0),
        (0x28, AddressingModes.IMPLIED, 4, instruction_PLP),
        (0x29, AddressingModes.IMMEDIATE, 2, instruction_AND),
        (0x2a, AddressingModes.ACCUMULATOR, 2, instruction_ROL),
        (0,0),
        (0x2c, AddressingModes.ABSOLUTE, 4, instruction_BIT),
        (0x2d, AddressingModes.ABSOLUTE, 4, instruction_AND),
        (0x2e, AddressingModes.ABSOLUTE, 6, instruction_ROL),
        (0,0),
        (0x30, AddressingModes.RELATIVE, 2, instruction_BMI),
        (0x31, AddressingModes.INDIRECT_INDEXED, 5, instruction_AND),
        (0,0),
        (0,0),
        (0,0),
        (0x35, AddressingModes.INDEXED_ZERO_PAGE_X, 4, instruction_AND),
        (0x36, AddressingModes.INDEXED_ZERO_PAGE_X, 6, instruction_ROL),
        (0,0),
        (0x38, AddressingModes.IMPLIED, 2, instruction_SEC),
        (0x39, AddressingModes.INDEXED_ABSOLUTE_Y, 4, instruction_AND),
        (0,0),
        (0,0),
        (0,0),
        (0x3d, AddressingModes.INDEXED_ABSOLUTE_X, 4, instruction_AND),
        (0x3e, AddressingModes.INDEXED_ABSOLUTE_X, 7, instruction_ROL),
        (0,0),
        (0x40, AddressingModes.IMPLIED, 6, instruction_RTI),
        (0x41, AddressingModes.INDEXED_INDIRECT, 5, instruction_EOR),
        (0,0),
        (0,0),
        (0,0),
        (0x45, AddressingModes.ZERO_PAGE, 3, instruction_EOR),
        (0x46, AddressingModes.ZERO_PAGE, 5, instruction_LSR),
        (0,0),
        (0x48, AddressingModes.IMPLIED, 3, instruction_PHA),
        (0x49, AddressingModes.IMMEDIATE, 2, instruction_EOR),
        (0x4a, AddressingModes.ACCUMULATOR, 2, instruction_LSR),
        (0,0),
        (0x4c, AddressingModes.ABSOLUTE, 3, instruction_JMP),
        (0x4d, AddressingModes.ABSOLUTE, 4, instruction_EOR),
        (0x4e, AddressingModes.ABSOLUTE, 6, instruction_LSR),
        (0,0),
        (0x50, AddressingModes.RELATIVE, 2, instruction_BVC),
        (0x51, AddressingModes.INDIRECT_INDEXED, 6, instruction_EOR),
        (0,0),
        (0,0),
        (0,0),
        (0x55, AddressingModes.INDEXED_ZERO_PAGE_X, 4, instruction_EOR),
        (0x56, AddressingModes.INDEXED_ZERO_PAGE_X, 6, instruction_LSR),
        (0,0),
        (0x58, AddressingModes.IMPLIED, 2, instruction_CLI),
        (0x59, AddressingModes.INDEXED_ABSOLUTE_Y, 4, instruction_EOR),
        (0,0),
        (0,0),
        (0,0),
        (0x5d, AddressingModes.INDEXED_ABSOLUTE_X, 4, instruction_EOR),
        (0x5e, AddressingModes.INDEXED_ABSOLUTE_X, 7, instruction_LSR),
        (0,0),
        (0x60, AddressingModes.IMPLIED, 6, instruction_RTS),
        (0x61, AddressingModes.INDEXED_INDIRECT, 6, instruction_ADC),
        (0,0),
        (0,0),
        (0,0),
        (0x65, AddressingModes.ZERO_PAGE, 3, instruction_ADC),
        (0x66, AddressingModes.ZERO_PAGE, 5, instruction_ROR),
        (0,0),
        (0x68, AddressingModes.IMPLIED, 4, instruction_PLA),
        (0x69, AddressingModes.IMMEDIATE, 2, instruction_ADC),
        (0x6a, AddressingModes.ACCUMULATOR, 2, instruction_ROR),
        (0,0),
        (0x6c, AddressingModes.INDIRECT, 5, instruction_JMP),
        (0x6d, AddressingModes.ABSOLUTE, 4, instruction_ADC),
        (0x6e, AddressingModes.ABSOLUTE, 6, instruction_ROR),
        (0,0),
        (0x70, AddressingModes.RELATIVE, 2, instruction_BVS),
        (0x71, AddressingModes.INDIRECT_INDEXED, 5, instruction_ADC),
        (0,0),
        (0,0),
        (0,0),
        (0x75, AddressingModes.INDEXED_ZERO_PAGE_X, 4, instruction_ADC),
        (0x76, AddressingModes.INDEXED_ZERO_PAGE_X, 6, instruction_ROR),
        (0,0),
        (0x78, AddressingModes.IMPLIED, 2, instruction_SEI),
        (0x79, AddressingModes.INDEXED_ABSOLUTE_Y, 4, instruction_ADC),
        (0,0),
        (0,0),
        (0,0),
        (0x7d, AddressingModes.INDEXED_ABSOLUTE_X, 4, instruction_ADC),
        (0x7e, AddressingModes.INDEXED_ABSOLUTE_X, 7, instruction_ROR),
        (0,0),
        (0,0),
        (0x81, AddressingModes.INDEXED_INDIRECT, 6, instruction_STA),
        (0,0),
        (0,0),
        (0x84, AddressingModes.ZERO_PAGE, 3, instruction_STY),
        (0x85, AddressingModes.ZERO_PAGE, 3, instruction_STA),
        (0x86, AddressingModes.ZERO_PAGE, 3, instruction_STX),
        (0,0),
        (0x88, AddressingModes.IMPLIED, 2, instruction_DEY),
        (0,0),
        (0x8a, AddressingModes.IMPLIED, 2, instruction_TXA),
        (0,0),
        (0x8c, AddressingModes.ABSOLUTE, 4, instruction_STY),
        (0x8d, AddressingModes.ABSOLUTE, 4, instruction_STA),
        (0x8e, AddressingModes.ABSOLUTE, 4, instruction_STX),
        (0,0),
        (0x90, AddressingModes.RELATIVE, 2, instruction_BCC),
        (0x91, AddressingModes.INDIRECT_INDEXED, 6, instruction_STA),
        (0,0),
        (0,0),
        (0x94, AddressingModes.INDEXED_ZERO_PAGE_X, 4, instruction_STY),
        (0x95, AddressingModes.INDEXED_ZERO_PAGE_X, 4, instruction_STA),
        (0x96, AddressingModes.INDEXED_ZERO_PAGE_Y, 4, instruction_STX),
        (0,0),
        (0x98, AddressingModes.IMPLIED, 2, instruction_TYA),
        (0x99, AddressingModes.INDEXED_ABSOLUTE_Y, 5, instruction_STA),
        (0x9a, AddressingModes.IMPLIED, 2, instruction_TXS),
        (0,0),
        (0,0),
        (0x9d, AddressingModes.INDEXED_ABSOLUTE_X, 5, instruction_STA),
        (0,0),
        (0,0),
        (0xa0, AddressingModes.IMMEDIATE, 2, instruction_LDY),
        (0xa1, AddressingModes.INDEXED_INDIRECT, 6, instruction_LDA),
        (0xa2, AddressingModes.IMMEDIATE, 2, instruction_LDX),
        (0,0),
        (0xa4, AddressingModes.ZERO_PAGE, 3, instruction_LDY),
        (0xa5, AddressingModes.ZERO_PAGE, 3, instruction_LDA),
        (0xa6, AddressingModes.ZERO_PAGE, 3, instruction_LDX),
        (0,0),
        (0xa8, AddressingModes.IMPLIED, 2, instruction_TAY),
        (0xa9, AddressingModes.IMMEDIATE, 2, instruction_LDA),
        (0xaa, AddressingModes.IMPLIED, 2, instruction_TAX),
        (0,0),
        (0xac, AddressingModes.ABSOLUTE, 4, instruction_LDY),
        (0xad, AddressingModes.ABSOLUTE, 4, instruction_LDA),
        (0xae, AddressingModes.ABSOLUTE, 4, instruction_LDX),
        (0,0),
        (0xb0, AddressingModes.RELATIVE, 2, instruction_BCS),
        (0xb1, AddressingModes.INDIRECT_INDEXED, 5, instruction_LDA),
        (0,0),
        (0,0),
        (0xb4, AddressingModes.INDEXED_ZERO_PAGE_X, 4, instruction_LDY),
        (0xb5, AddressingModes.INDEXED_ZERO_PAGE_X, 4, instruction_LDA),
        (0xb6, AddressingModes.INDEXED_ZERO_PAGE_Y, 4, instruction_LDX),
        (0,0),
        (0xb8, AddressingModes.IMPLIED, 2, instruction_CLV),
        (0xb9, AddressingModes.INDEXED_ABSOLUTE_Y, 4, instruction_LDA),
        (0xba, AddressingModes.IMPLIED, 2, instruction_TSX),
        (0,0),
        (0xbc, AddressingModes.INDEXED_ABSOLUTE_X, 4, instruction_LDY),
        (0xbd, AddressingModes.INDEXED_ABSOLUTE_X, 4, instruction_LDA),
        (0xbe, AddressingModes.INDEXED_ABSOLUTE_Y, 4, instruction_LDX),
        (0,0),
        (0xc0, AddressingModes.IMMEDIATE, 2, instruction_CPY),
        (0xc1, AddressingModes.INDEXED_INDIRECT, 5, instruction_CMP),
        (0,0),
        (0,0),
        (0xc4, AddressingModes.ZERO_PAGE, 3, instruction_CPY),
        (0xc5, AddressingModes.ZERO_PAGE, 3, instruction_CMP),
        (0xc6, AddressingModes.ZERO_PAGE, 5, instruction_DEC),
        (0,0),
        (0xc8, AddressingModes.IMPLIED, 2, instruction_INY),
        (0xc9, AddressingModes.IMMEDIATE, 2, instruction_CMP),
        (0xca, AddressingModes.IMPLIED, 2, instruction_DEX),
        (0,0),
        (0xcc, AddressingModes.ABSOLUTE, 4, instruction_CPY),
        (0xcd, AddressingModes.ABSOLUTE, 4, instruction_CMP),
        (0xce, AddressingModes.ABSOLUTE, 6, instruction_DEC),
        (0,0),
        (0xd0, AddressingModes.RELATIVE, 2, instruction_BNE),
        (0xd1, AddressingModes.INDIRECT_INDEXED, 6, instruction_CMP),
        (0,0),
        (0,0),
        (0,0),
        (0xd5, AddressingModes.INDEXED_ZERO_PAGE_X, 4, instruction_CMP),
        (0xd6, AddressingModes.INDEXED_ZERO_PAGE_X, 6, instruction_DEC),
        (0,0),
        (0xd8, AddressingModes.IMPLIED, 2, instruction_CLD),
        (0xd9, AddressingModes.INDEXED_ABSOLUTE_Y, 4, instruction_CMP),
        (0,0),
        (0,0),
        (0,0),
        (0xdd, AddressingModes.INDEXED_ABSOLUTE_X, 4, instruction_CMP),
        (0xde, AddressingModes.INDEXED_ABSOLUTE_X, 7, instruction_DEC),
        (0,0),
        (0xe0, AddressingModes.IMMEDIATE, 2, instruction_CPX),
        (0xe1, AddressingModes.INDEXED_INDIRECT, 5, instruction_SBC),
        (0,0),
        (0,0),
        (0xe4, AddressingModes.ZERO_PAGE, 3, instruction_CPX),
        (0xe5, AddressingModes.ZERO_PAGE, 3, instruction_SBC),
        (0xe6, AddressingModes.ZERO_PAGE, 5, instruction_INC),
        (0,0),
        (0xe8, AddressingModes.IMPLIED, 2, instruction_INX),
        (0xe9, AddressingModes.IMMEDIATE, 2, instruction_SBC),
        (0xea, AddressingModes.IMPLIED, 2, instruction_NOP),
        (0,0),
        (0xec, AddressingModes.ABSOLUTE, 4, instruction_CPX),
        (0xed, AddressingModes.ABSOLUTE, 4, instruction_SBC),
        (0xee, AddressingModes.ABSOLUTE, 6, instruction_INC),
        (0,0),
        (0xf0, AddressingModes.RELATIVE, 2, instruction_BEQ),
        (0xf1, AddressingModes.INDIRECT_INDEXED, 6, instruction_SBC),
        (0,0),
        (0,0),
        (0,0),
        (0xf5, AddressingModes.INDEXED_ZERO_PAGE_X, 4, instruction_SBC),
        (0xf6, AddressingModes.INDEXED_ZERO_PAGE_X, 6, instruction_INC),
        (0,0),
        (0xf8, AddressingModes.IMPLIED, 2, instruction_SED),
        (0xf9, AddressingModes.INDEXED_ABSOLUTE_Y, 4, instruction_SBC),
        (0,0),
        (0,0),
        (0,0),
        (0xfd, AddressingModes.INDEXED_ABSOLUTE_X, 4, instruction_SBC),
        (0xfe, AddressingModes.INDEXED_ABSOLUTE_X, 7, instruction_INC),
        (0,0)
#        (0x00, AddressingModes.IMPLIED, 7, instruction_BRK),
        ]

def nmi():
    registers.nmi_pending = True

def step_one():
    if (registers.nmi_pending):
        instruction_BRK(AddressingModes.IMPLIED, nmi=True)
        registers.nmi_pending = False
        return 7 #cycles for brk
    instruction = memory.readAddress(registers.PC)
    element = InstructionMap[instruction]
    if (len(element) == 4):
        registers.current_instruction = registers.PC
        registers.PC += get_size(element[1])
        size = get_size(element[1])
#        print(element[3].__name__ + ":")
#        print("", format(registers.current_instruction, 'x').upper(), end = "  ")
#        for i in range(3):
#            if (size > i):
#                print('{0:0{1}x}'.format(memory.readAddress(registers.current_instruction + i), 2).upper(), end = " ")
#            else:
#                print("  ", end = " ")
#        print("A:" + '{0:0{1}x}'.format(registers.A, 2).upper(), end = " ")
#        print("X:" + '{0:0{1}x}'.format(registers.X, 2).upper(), end = " ")
#        print("Y:" + '{0:0{1}x}'.format(registers.Y, 2).upper(), end = " ")
#        print("P:" + '{0:0{1}x}'.format(registers.S, 2).upper(), end = " ")
#        print("SP:" + '{0:0{1}x}'.format(registers.SP, 2).upper())       
        extra_cycles = element[3](element[1]) #Instruction (AddressingModes.<MODE>). Returns pc increment


        return element[2] + extra_cycles
    print("INVALID INSTRUCTION:", hex(instruction))
    return 0

def step(amount):
    cycles = 0
    for i in range(amount):
        ret = step_one()
        if (ret != 0):
            cycles += ret
        else:
            return 0 #Instruction failed

    return cycles

def cycle():
    return step_one()
